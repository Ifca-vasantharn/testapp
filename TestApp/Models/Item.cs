﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TestApp.Models
{
    public class Item
    {
        public int id { get; set; }
        public string itemname { get; set; }
        public double price { get; set; }
    }
}
