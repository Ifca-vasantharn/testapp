﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Models;

namespace TestApp.Services
{
    public interface IItemService
    {
        List<Item> GetItem(int? id);
        List<Item> AddItem(Item item);

    }
}
