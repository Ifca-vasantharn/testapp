﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TestApp.Models;
using TestApp.DB;
namespace TestApp.Services
{
    public class ItemService : IItemService
    {
        public List<Item> AddItem(Item item)
        {
            var DBq = DB.DBitem.AddItem(item);
            return DBq;

        }

        public List<Item> GetItem(int? id)
        {
            var DBq = DB.DBitem.GetItem(id);
            return DBq;
        }

    }
}
