﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TestApp.Models;
using TestApp.Services;

namespace TestApp.Controllers
{
    [Route("/api")]
    [ApiController]
    public class ItemController : ControllerBase
    {
        private readonly IItemService _services;
        public ItemController(IItemService services)
        {
            _services = services;
        }

        [HttpPost("add")]
        public IActionResult AddItem([FromBody] Item item)
        {
            Console.Write("asd");
            System.Diagnostics.Debug.WriteLine("asdsad");
            var _Items = _services.AddItem(item);
            //throw new Exception("est");
            return Ok(_Items);
        }

        [HttpGet("get")]
        public IActionResult GetItem([FromQuery]int? id)
        {
            var _Items = _services.GetItem(id);
            return Ok(_Items);
        }
    }
}